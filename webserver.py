import pytest

# 1. Проверка установки CERTBOT.
def test_certbot_is_installed(host):
    certbot = host.package("certbot")
    assert certbot.is_installed

# 2. Проверка переноса файлов сайта и виртуальных хостов.
def test_nginx_files_and_conf(host):
    assert host.file("/home/nginxuser/wwwfiles/index.html").exists
    assert host.file("/etc/nginx/sites-available/chigur").exists
    assert host.file("/etc/nginx/sites-enabled/chigur").is_symlink

# 3. Проверка наличи изменений в конфигурации NGINX.
def test_nginx_conf(host):
    command = host.run("grep custom_format /etc/nginx/nginx.conf")
    assert command.rc == 0
    assert "custom_format" in command.stdout

# 4. Проверка статуса службы NGINX.
def test_nginx_service(host):
    assert host.service("nginx").is_enabled
    assert host.service("nginx").is_running